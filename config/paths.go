package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Config struct {
	AppDataOverride string   `json:"app_data_override"`
	Gw2ExePath      string   `json:"gw2_executable_path"`
	LaunchArguments []string `json:"launch_arguments"`
}

var (
	UserConfig    *Config
	AppData       string
	Gw2FolderPath string
	Gw2FolderName = "Guild Wars 2"
)

func Init() {
	file, _ := ioutil.ReadFile("config.json")

	if err := json.Unmarshal([]byte(file), &UserConfig); err != nil {
		panic(err)
	}

	if UserConfig.AppDataOverride != "" {
		AppData = UserConfig.AppDataOverride
	} else {
		AppData = os.Getenv("APPDATA")
	}

	Gw2FolderPath = AppData + "\\" + Gw2FolderName
}

package utils

import (
	"os"
	"fmt"
	"strconv"
	"strings"
	"bufio"
)

func AskForInt(question string) int {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(question + " ")
	choose, _ := reader.ReadString('\n')

	index, err := strconv.Atoi(strings.ReplaceAll(choose, "\r\n", ""))
	if err != nil {
		fmt.Print(err)
		fmt.Println("ERR: Please enter a valid number.")
		return AskForInt(question)
	}
	return index
}
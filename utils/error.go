package utils

import (
	"log"
)

func IsErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

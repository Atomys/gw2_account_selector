package utils

import (
	"gitlab.com/Atomys/gw2_account_selector/config"
	"io"
	"io/ioutil"
	"os"
)

func Copy(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}

func Gw2FolderExist() (gw2FolderExist bool) {
	for _, f := range ListFiles(os.Getenv("APPDATA")) {
		if f.Name() == config.Gw2FolderName && f.IsDir() {
			gw2FolderExist = true
			break
		}
	}
	return
}

func ListFiles(path string) (files []os.FileInfo) {
	files, err := ioutil.ReadDir(path)
	IsErr(err)
	return
}

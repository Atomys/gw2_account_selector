package main

import (
	"fmt"
	"gitlab.com/Atomys/gw2_account_selector/config"
	"gitlab.com/Atomys/gw2_account_selector/profile"
	"gitlab.com/Atomys/gw2_account_selector/utils"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

func launchGW2(p *profile.Profile) {
	fmt.Printf(
		"Copying profile %s to %s ...\n",
		strings.ReplaceAll(p.Path, os.Getenv("APPDATA"), ""),
		strings.ReplaceAll(config.Gw2FolderPath+"\\Local.dat", os.Getenv("APPDATA"), ""),
	)
	utils.Copy(p.Path, config.Gw2FolderPath+"\\Local.dat")

	arguments := []string{"-autologin"}
	if len(config.UserConfig.LaunchArguments) > 0 {
		fmt.Printf("Applying extra arguments to launcher: %+v\n", config.UserConfig.LaunchArguments)
		arguments = append(arguments, config.UserConfig.LaunchArguments...)
	}

	fmt.Printf("\nLaunch Guild Wars 2 with %s account\n", p.AccountName)
	c := exec.Command("cmd", "/C", "start", "", config.UserConfig.Gw2ExePath, strings.Join(arguments, " "))

	if err := c.Run(); err != nil {
		fmt.Println("Run Error: ", err)
	}

	fmt.Println("Guild Wars 2 succesfully started !")
}

func clearTerminal() {
	var cmd *exec.Cmd

	switch runtime.GOOS {
	case "linux":
		cmd = exec.Command("clear")
	case "windows":
		cmd = exec.Command("cmd", "/c", "cls")
	}
	cmd.Stdout = os.Stdout
	cmd.Run()
}

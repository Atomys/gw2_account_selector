package profile

import (
	"regexp"
	"sort"
	"strconv"

	"gitlab.com/Atomys/gw2_account_selector/config"
	"gitlab.com/Atomys/gw2_account_selector/utils"
)

type Profile struct {
	Path        string
	Index       int
	AccountName string
	World       string
}

var (
	Profiles []*Profile
)

func Load() {
	for _, f := range utils.ListFiles(config.Gw2FolderPath) {
		if p := extractProfil(f.Name()); p != nil {
			Profiles = append(Profiles, p)
		}
	}

	sort.Slice(Profiles, func(i, j int) bool {
		return Profiles[i].Index < Profiles[j].Index
	})
}

func extractProfil(fileName string) *Profile {
	re := regexp.MustCompile(`(?P<Index>\d{1,})?\.?Local\.(?P<World>[A-Za-z-_]{2,})\.(?P<AccountName>[A-Za-z0-9.]{2,})\.dat`)
	match := re.FindStringSubmatch(fileName)
	if len(match) == 0 {
		return nil
	}

	result := make(map[string]string)
	for i, name := range re.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = match[i]
		}
	}

	var index int
	var err error
	if index, err = strconv.Atoi(result["Index"]); err != nil {
		index = 1000000
	}

	return &Profile{
		Path:        config.Gw2FolderPath + "\\" + fileName,
		Index:       index,
		AccountName: result["AccountName"],
		World:       result["World"],
	}

}

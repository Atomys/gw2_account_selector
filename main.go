package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/Atomys/gw2_account_selector/config"
	"gitlab.com/Atomys/gw2_account_selector/profile"
	"gitlab.com/Atomys/gw2_account_selector/utils"
)

type Profile struct {
	Path        string
	AccountName string
	WorldSide   string
}

func init() {
	config.Init()
}

func main() {
	clearTerminal()

	if !utils.Gw2FolderExist() {
		log.Fatal("Guild Wars 2 isn't installed on your computer !")
	}
	fmt.Println("Loading profiles...")
	profile.Load()

	if len(profile.Profiles) == 0 {
		fmt.Println("You don't have any profile. Exit...")
		time.Sleep(10 * time.Second)
		os.Exit(0)
	}

	fmt.Printf("Available profiles:\n")
	for i, p := range profile.Profiles {
		fmt.Printf("%d :\n", i)
		fmt.Printf("	Server: %s\n", p.World)
		fmt.Printf("	AccountName: %s\n\n", p.AccountName)
	}

	launchGW2(profile.Profiles[utils.AskForInt("With whom do you want to connect?")])
	time.Sleep(2 * time.Second)
}

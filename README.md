# GW2 Account Selector

A very basic multi account selector based on the new way after shortcuts with the -email and -password command line arguments was removed in the January 22, 2019 update.

# Usage

To create a new profile :

 1. Start the Guild Wars 2 client. 
 2. Enter your login details, clicking the "Remember Account Name" and the "Remember Password" checkboxes. Continue to character select
 3. Close the client.
 4. Open the following folder: `%APPDATA%\Guild Wars 2`
 5. Create a copy of the "Local.dat" file named `Local.<EU/US>.<AccountName>.dat`.  This resolves to something like `Local.EU.Atomys.1234.dat`

To start :
 1. Execute `gw2_account_selector.exe` and following instructions

# Build 

Only tested on Windows at the moment
Windows : `GOOS=windows GOARCH=amd64 go build`